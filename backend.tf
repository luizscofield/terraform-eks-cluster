terraform {
  backend "s3" {
    bucket = "luizscofield-terraform"
    region = "us-east-1"
    key    = "tf-eks-backend/terraform.tfstate"
  }
}