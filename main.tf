module "eks-cluster" {
  source              = "github.com/luizscofield/terraform-eks?ref=v1.0"
  project_name        = var.project_name
  region              = var.region
  node-group-max-size = var.node-group-max-size
  instance_types      = var.instance_types
  global_tags         = local.tags
}